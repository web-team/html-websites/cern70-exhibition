document.addEventListener("DOMContentLoaded", function () {
  var savedLang = localStorage.getItem("language");
  if (savedLang) {
    applyLanguage(savedLang);
    document.getElementById("languageSelector").value = savedLang;
  } else {
    applyLanguage("en");
    document.getElementById("languageSelector").value = "en";
  }
});

function changeLanguage() {
  var selectedLang = document.getElementById("languageSelector").value;
  applyLanguage(selectedLang);
  localStorage.setItem("language", selectedLang);
}

function applyLanguage(lang) {
  var contentFR = document.getElementsByClassName("lang-fr");
  var contentEN = document.getElementsByClassName("lang-en");
  var contentDE = document.getElementsByClassName("lang-de");
  var contentES = document.getElementsByClassName("lang-es");
  var contentGR = document.getElementsByClassName("lang-gr");

  for (var i = 0; i < contentFR.length; i++) {
    contentFR[i].style.display = lang === "fr" ? "block" : "none";
  }
  for (var j = 0; j < contentEN.length; j++) {
    contentEN[j].style.display = lang === "en" ? "block" : "none";
  }
  for (var j = 0; j < contentDE.length; j++) {
    contentDE[j].style.display = lang === "de" ? "block" : "none";
  }
  for (var k = 0; k < contentES.length; k++) {
    contentES[k].style.display = lang === "es" ? "block" : "none";
  }
  for (var l = 0; l < contentGR.length; l++) {
    contentGR[l].style.display = lang === "gr" ? "block" : "none";
  }
}
